#pragma once
#include "stadfx.h"

class User;

class Room
{
public:
	Room();
	Room(int id, User* admin, string name, int maxUsers, int questionNo, int questionTime);
	bool joinRoom(User* user);
	void leaveRoom(User* user);
	int closeRoom(User* user);
	vector<User*> getUsers();
	string getUsersListMessage();
	int getQuistionNo();
	int getId();
	string getName();
	~Room();
private:
	string getUsersAsString(std::vector<User*> usersList, User* excludeUser);
	void sendMessage(User* excludeUser, string message);
	void sendMessage(string message);

	std::vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	string _name;
	int _id;
};

