#include "Question.h"

Question::Question()
{
}


Question::Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4)
{
	this->_id = id;
	this->_question = question;

	srand(time(NULL));
	int indexes[4];
	while (indexes[0] != indexes[1] && indexes[0] != indexes[2] && indexes[0] != indexes[4] && indexes[1] != indexes[2] && indexes[1] != indexes[3] && indexes[2] != indexes[3])
	{
		indexes[0] = rand() % 4;
		indexes[1] = rand() % 4;
		indexes[2] = rand() % 4;
		indexes[3] = rand() % 4;
	}

	this->_correctIndex = indexes[0];
	this->_answers[indexes[0]] = correctAnswer;
	this->_answers[indexes[1]] = answer2;
	this->_answers[indexes[2]] = answer3;
	this->_answers[indexes[3]] = answer4;
}

string Question::getQuestion()
{
	return this->_question;
}

string* Question::getAnswers()
{
	return this->_answers;
}

int Question::getCorrectAnswerIndex()
{
	return this->_correctIndex;
}

int Question::getId()
{
	return this->_id;
}