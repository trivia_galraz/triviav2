#pragma once
#include "stadfx.h"
#include "DataBase.h"
#include "Question.h"
#include "User.h"

class User;

class Game
{
public:
	Game(const vector<User*>& players, int questionsNo, DataBase& db);
	void sendQuestionToAllUsers();
	void handleFinishGame();
	void sendFirstQuestion();
	bool handleNextTurn();
	bool handleAnswerFromUser(User* user, int answerNo, int time);
	bool leaveGame(User* currUser);
	~Game();
private:	
	bool insertGameToDB();
	void initQuestionsFromDB();
	void sendQuestionsToAllUsers();

	vector<Question*> _questions;
	vector<User*> _players;
	int _questionNo;
	int _currQuestionIndex;	
	DataBase& _db;
	map<string, int> _results;
	int _currentTurnAnswers;
};

