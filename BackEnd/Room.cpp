#include "Room.h"
#include "User.h"

Room::Room()
{
}

Room::~Room()
{
}

Room::Room(int id, User* admin, string name, int maxUsers, int questionNo, int questionTime)
{
	this->_id = id;
	this->_admin = admin;
	this->_name = name;
	this->_maxUsers = maxUsers;
	this->_questionNo = questionNo;
	this->_questionTime = questionTime;
	this->_users.insert(this->_users.begin(), this->_admin);
}

string Room::getUsersAsString(std::vector<User*> usersList, User* excludeUser)
{
	string toReturn = "";

	for (std::vector<User*>::size_type i = 1; i != this->_users.size(); i++)
	{
		if (_users[i] == excludeUser)
		{
			break;
		}

		toReturn += Helper::getPaddedNumber(this->_users[i]->getUsername().length, 2);
		toReturn += this->_users[i]->getUsername();
	}

	return toReturn;
}

void Room::sendMessage(User* excludeUser, string message)
{
	for (std::vector<User*>::size_type i = 0; i != this->_users.size(); i++)
	{
		if (_users[i] == excludeUser)
		{
			break;
		}

		this->_users[i]->send(message);
	}
}

void Room::sendMessage(string message)
{
	this->sendMessage(NULL, message);
}

bool Room::joinRoom(User* user)
{
	if (this->_users.size() == this->_maxUsers)
	{
		return false;
	}

	this->_users.push_back(user);
	return true;
}

void Room::leaveRoom(User* user) 
{
	int index = find(this->_users.begin(), this->_users.end(), user) - this->_users.begin();
	if (index <= this->_users.size())
	{
		this->_users.at(index)->send(std::to_string(LEAVE_ROOM_RESPONSE) + "0");
		this->_users.erase(this->_users.begin() + index);
		this->sendMessage(this->getUsersListMessage());
	}
}

int Room::closeRoom(User* user)
{
	if (user != this->_admin)
	{
		return -1;
	}

	for (std::vector<User*>::size_type i = 0; i != this->_users.size(); i++)
	{
		this->_users[i]->send(std::to_string(CLOSE_ROOM_RESPONSE));

		if (this->_users[i]->getUsername() != this->_admin->getUsername())
		{
			this->_users[i]->clearRoom();
		}
	}

	return this->_id;
}

vector<User*> Room::getUsers()
{
	return this->_users;
}

string Room::getUsersListMessage()
{
	return "108" + this->_users.size() + this->getUsersAsString(this->_users, nullptr);
}

int Room::getQuistionNo()
{
	return this->_questionNo;
}

int Room::getId()
{
	return this->_id;
}

string Room::getName()
{
	return this->_name;
}