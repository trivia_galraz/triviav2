#include "TriviaServer.h"

/*
*create a new socket
*input: none
*output: none
*/
TriviaServer::TriviaServer()
{
	this->_socket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (this->_socket == INVALID_SOCKET)
	{
		throw exception("Error can't create a socket");
	}
}
/*
*close the socket
*input: none
*output: none
*/
TriviaServer::~TriviaServer()
{
	try
	{
		::closesocket(_socket);
	}
	catch (...) {}
}
/*
*this function is the main thread of the server
*input: none
*output: none
*/
void TriviaServer::server()
{
	thread* handle = new thread(&TriviaServer::handleRecievedMessages, this);
	try
	{
		bindAndListen();
	}
	catch (const exception& e)
	{
		throw e;
	}
	std::cout << "Listening to new clients" << std::endl;
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		try
		{
			accept();

		}
		catch (const exception& e)
		{
			throw e;
		}

	}
}
/*
*binding and listening 
*input: none
*output: none
*/
void TriviaServer::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (::bind(this->_socket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
	{
		throw exception("Error binding the socket");
	}
	// Start listening for incoming requests of clients
	if (::listen(this->_socket, SOMAXCONN) == SOCKET_ERROR)
	{
		throw exception("Error listening to the socket");
	}
}

/*
*accept the user and creating a new user socket
*input: none
*output: none
*/
void TriviaServer::accept()
{
	// this accepts the client and create a specific socket from server to this client
	SOCKET clientSocket = ::accept(this->_socket, NULL, NULL);
	std::cout << "new client conected" << std::endl;
	if (clientSocket == INVALID_SOCKET)
	{
		throw exception("Error accepting the user");
	}
	// the function that handle the conversation with the client
	thread* handle = new thread(&TriviaServer::clientHandler, this, clientSocket);
	this->_handlers.push_back(handle);
}

/*
this function is the communication between the server and the client
the function build and add message to the messages queue
input: the socket of the client
output: none
*/
void TriviaServer::clientHandler(SOCKET socket)
{
	try
	{
		int typeCode = Helper::getMessageTypeCode(socket);
		while (typeCode != LEAVE_APP_REQUEST && typeCode != 0)
		{
			cout << "new message added to the queue" << endl;
			addRecievedMessages(buildRecieveMessage(socket, typeCode));
			this->_queueEmpty.notify_one();
			typeCode = Helper::getMessageTypeCode(socket);
		}
	}
	catch (const std::exception& e)
	{
		cout << "sorry somthing went wrong" << endl;
		cout << e.what() << endl;
		addRecievedMessages(buildRecieveMessage(socket, LEAVE_APP_REQUEST));
	}
	cout << "close socket" << endl;
	closesocket(socket); //closing the socket
	
}
/*
this function handle the recieved messages queue
the function allways runing in the back and awake when new
messages enters the queue
input: none
output: none
*/
void TriviaServer::handleRecievedMessages()
{
	RecievedMessage* message;
	try
	{
		std::unique_lock<std::mutex> lk(this->_mtxRecivedMessages);
		while (true)
		{
			this->_queueEmpty.wait(lk);//wait untill messages will enter to the queue
			while (!this->_queRcvMessages.empty())
			{
				message = this->_queRcvMessages.front();
				this->_queRcvMessages.pop();
				std::cout << "data for message " + std::to_string(message->getMessageCode()) << std::endl;
				switch (message->getMessageCode()) //switch the type code and handle 
				{
				case SIGN_IN_REQUEST:
					this->_connectedUsers[message->getSock()] = handleSignin(message);
					break;
				case SIGN_OUT_REQUEST:
					handleSignout(message);
					break;
				case SIGN_UP_REQUEST:
					handleSignup(message);
					break;
				case LEAVE_GAME_REQUEST:
					handleLeaveGame(message);
					break;
				default:
					cout << "request not supported yet." << endl;
					break;
				}
			}
		}

	}
	catch (exception& e)
	{
		cout << "sorry somthing went wrong" << endl;
		cout << e.what() << endl;
	}
}
/*
this function adding messages to the queue
the function locks the queue mutex and add the message
input: message
output: none
*/
void TriviaServer::addRecievedMessages(RecievedMessage * message)
{
	this->_mtxRecivedMessages.lock();
	this->_queRcvMessages.push(message);
	this->_mtxRecivedMessages.unlock();
}
/*
this function build recive message using the protocol
the type code and the socket
input: socket and type code
output: new Recived message
*/
RecievedMessage * TriviaServer::buildRecieveMessage(SOCKET socket, int typeCode)
{
	RecievedMessage* message;
	vector<string>* data = new vector<string>();
	string s;
	switch (typeCode)
	{
	case SIGN_IN_REQUEST:
		s = Helper::getStringPartFromSocket(socket, Helper::getIntPartFromSocket(socket, 2));
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, Helper::getIntPartFromSocket(socket, 2));
		data->push_back(s);
		message = new RecievedMessage(socket, typeCode, *data);
		break;
	case SIGN_UP_REQUEST:
		s = Helper::getStringPartFromSocket(socket, Helper::getIntPartFromSocket(socket, 2));
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, Helper::getIntPartFromSocket(socket, 2));
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, Helper::getIntPartFromSocket(socket, 2));
		data->push_back(s);
		message = new RecievedMessage(socket, typeCode, *data);
		break;
	case ROOM_USERS_REQUEST:
		s = Helper::getStringPartFromSocket(socket, 4);
		data->push_back(s);
		message = new RecievedMessage(socket, typeCode, *data);
		break;
	case JOIN_ROOM_REQUEST:
		s = Helper::getStringPartFromSocket(socket, 4);
		data->push_back(s);
		message = new RecievedMessage(socket, typeCode, *data);
		break;
	case CREATE_ROOM_REQUEST:
		s = Helper::getStringPartFromSocket(socket, Helper::getIntPartFromSocket(socket, 2));
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, 1);
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, 2);
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, 2);
		data->push_back(s);
		message = new RecievedMessage(socket, typeCode, *data);
		break;
	case CHECK_ANSWER_REQUEST:
		s = Helper::getStringPartFromSocket(socket, 1);
		data->push_back(s);
		s = Helper::getStringPartFromSocket(socket, 2);
		data->push_back(s);
		message = new RecievedMessage(socket, typeCode, *data);
		break;
	default:
		message = new RecievedMessage(socket, typeCode);
		break;
	}
	return message;
}
void TriviaServer::safeDeleteUser(RecievedMessage *)
{

}
/*
this function handles sign in case
input: the sign in message
output: the new user
*/
User * TriviaServer::handleSignin(RecievedMessage * msg)
{
	/*if (!this->_db.isUserAndPassMatch(msg->getValues()[0], msg->getValues()[0]))
	{
		Helper::sendData(msg->getSock(),(SIGN_IN_RESPONSE) + "1");
		return nullptr;
	}*/

	if (this->getUserByName(msg->getValues()[0]) != nullptr)
	{
		Helper::sendData(msg->getSock(), (SIGN_IN_RESPONSE)+"2");
		return nullptr;
	}

	User* user = new User(msg->getValues()[0], msg->getSock());
	user->send(std::to_string(SIGN_IN_RESPONSE) + "0");
	cout << "new user " + user->getUsername() + " added" << endl;
	return user;
}
/*
this function handles the sign up case
input: the sign up message 
output: true if the user signed, otherwise false
*/
bool TriviaServer::handleSignup(RecievedMessage * msg)
{
	string username = msg->getValues()[0];
	string password = msg->getValues()[1];
	string email = msg->getValues()[2];

	if (!Validator::isPasswordValid(password))
	{
		msg->getUser()->send(std::to_string(SIGN_UP_RESPONSE) + "1");
		return false;
	}

	if (!Validator::isUaernameValid(username))
	{
		msg->getUser()->send(std::to_string(SIGN_UP_RESPONSE) + "3");
		return false;
	}

	if (this->_db.isUserExists(msg->getUser()->getUsername()))
	{
		msg->getUser()->send(std::to_string(SIGN_UP_RESPONSE) + "2");
		return false;
	}

	bool addStatus = this->_db.addNewUser(username, password, email);
	if (!addStatus)
	{
		msg->getUser()->send(std::to_string(SIGN_UP_RESPONSE) + "4");
		return false;
	}

	msg->getUser()->send(std::to_string(SIGN_UP_RESPONSE) + "0");
	return true;
}
/*
this function handle sign out case
input: the sign out message
output: null
*/
void TriviaServer::handleSignout(RecievedMessage * msg)
{
	if (msg->getUser() != nullptr)
	{
		this->safeDeleteUser(msg);
		this->handleCloseRoom(msg);
		this->handleLeaveRoom(msg);
		this->handleLeaveGame(msg);
	}
}
/*

*/
void TriviaServer::handleLeaveGame(RecievedMessage *msg)
{
	if (msg->getUser()->leaveGame())
	{
		//need to free the game memory
	}
}

void TriviaServer::handleStartGame(RecievedMessage * msg)
{

}

void TriviaServer::handlePlayerAnswer(RecievedMessage *msg)
{
}

bool TriviaServer::handleCreateRoom(RecievedMessage *)
{
	return false;
}

bool TriviaServer::handleCloseRoom(RecievedMessage *)
{
	return false;
}

bool TriviaServer::handleJoinRoom(RecievedMessage *)
{
	return false;
}

bool TriviaServer::handleLeaveRoom(RecievedMessage *)
{
	return false;
}

void TriviaServer::handleGetUsersIn(RecievedMessage *)
{
}

void TriviaServer::handleGetRooms(RecievedMessage *)
{
}

void TriviaServer::handleGetBestScores(RecievedMessage *)
{
}

void TriviaServer::handleGetPersonalStatus(RecievedMessage *)
{
}
/*
this function return a user 
from the connected users list by username
input: string of the user name
output: the user from the list
*/
User * TriviaServer::getUserByName(string username)
{
	for (map<SOCKET, User*>::iterator it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); ++it)
	{
		if (it->second->getUsername() == username)
		{
			return it->second;
		}
	}
	return nullptr;
}
/*
this function return a user
from the connected user list by the socket
input: user socket
output: the user
*/
User * TriviaServer::getUserBySocket(SOCKET socket)
{
	for (map<SOCKET, User*>::iterator it = this->_connectedUsers.begin(); it != this->_connectedUsers.end(); ++it)
	{
		if (it->first == socket)
		{
			return it->second;
		}
	}
	return nullptr;
}

Room * TriviaServer::getRoomById(int)
{
	return nullptr;
}