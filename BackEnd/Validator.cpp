#include "Validator.h"

bool Validator::isPasswordValid(string password)
{
	bool aUpper = false;
	bool aLower = false;
	bool aDigit = false;
	
	//looking for at least one digit, one upper- case letter and onelower- case letter.
	for (int i = 0; password[i]; ++i)
	{
		if (isupper(password[i]))
		{
			aUpper = true;
		}
		else if (islower(password[i]))
		{
			aLower = true;
		}
		else if (isdigit(password[i]))
		{
			aDigit = true;
		}
	}

	if (aUpper && aLower && aDigit && !password.find(' '))
	{
		return true;
	}

	return false;
}

bool Validator::isUaernameValid(string username)
{
	if (username.length() == 0)
	{
		return false;
	}

	//checking if the first char is an alphabet
	if (!(username[0] > 64 && username[0] < 91) || (username[0] > 96 && username[0] < 123))
	{
		return false;
	}

	//checking if the username contains a space
	if (username.find(' '))
	{
		return false;
	}

	return true;
}