#include "Game.h"

Game::Game(const vector<User*>& players, int questionsNo, DataBase& db) : _db(db)
{
	//this->_db = db;
	int status = this->_db.insertNewGame();

	if (status == -1)
	{
		throw;
	}

	this->_db.initQuestions(this->_questionNo);
	this->_players = players;

	for (auto it = this->_players.begin(); it != this->_players.end(); it++)
	{
		(*it)->setGame(this);
	}
}

Game::~Game()
{
	this->_players.clear();
	this->_questions.clear();
}

void Game::sendQuestionToAllUsers()
{
	string toSend = std::to_string(SEND_QUESTIONS_RESPONSE) + Helper::getPaddedNumber(this->_questions[this->_questionNo]->getQuestion().length, 3) + this->_questions[this->_questionNo]->getQuestion();

	for (int i = 0; i < 4; i++)
	{
		toSend += Helper::getPaddedNumber(this->_questions[i]->getAnswers()[i].length, 3);
		toSend += this->_questions[i]->getAnswers()[i];
	}

	this->_currentTurnAnswers = 0;

	for (std::vector<User*>::size_type i = 0; i != this->_players.size(); i++)
	{
		try
		{
			this->_players[i]->send(toSend);
		}
		catch (exception& e)
		{
			cout << e.what() << endl;
		}
	}
}

void Game::handleFinishGame() //need to complete
{
	//this->_db.updateGameStatus();
	string toSend = std::to_string(LEAVE_GAME_RESPONSE) + std::to_string(this->_players.size());

	for (int i = 0; i < this->_players.size(); i++)
	{
		toSend += Helper::getPaddedNumber(this->_players[i]->getUsername().length, 2);
		toSend += this->_players[i]->getUsername();
		toSend += "score";
		toSend += Helper::getPaddedNumber(this->_results.find(this->_players[i]->getUsername())->second, 2);
	}

	for (std::vector<User*>::size_type i = 0; i != this->_players.size(); i++)
	{
		try
		{
			this->_players[i]->send(toSend);
		}
		catch (exception& e)
		{
			cout << e.what() << endl;
		}
	}
}

void Game::sendFirstQuestion()
{
	this->sendQuestionToAllUsers();
}

bool Game::handleNextTurn()
{
	if (this->_players.size() == 0)
	{
		this->handleFinishGame();
		return false;
	}

	if (this->_currentTurnAnswers == this->_players.size())
	{
		if (this->_currQuestionIndex = this->_questions.size() - 1)
		{
			this->handleFinishGame();
			return false;
		}

		this->_currQuestionIndex++;
		this->sendQuestionToAllUsers();
	}

	return true;
}

bool Game::handleAnswerFromUser(User* user, int answerNo, int time) //need to complete!
{
	this->_currentTurnAnswers++;

	if (answerNo - 1 == this->_questions[this->_currQuestionIndex]->getCorrectAnswerIndex())
	{
		this->_results.find(user->getUsername())->second++;
	}

	//this->_db.addAnswerToPlayer();
}