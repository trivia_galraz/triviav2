#pragma once
#include "stadfx.h"
#include <time.h>

class Question
{
public:
	Question();
	Question(int id, string question, string correctAnswer, string answer2, string answer3, string answer4);
	string getQuestion();
	string* getAnswers();
	int getCorrectAnswerIndex();
	int getId();
private:
	string _question;
	string _answers[4];
	int _correctIndex;
	int _id;
};

