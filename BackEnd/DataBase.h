#pragma once
#include "stadfx.h"
#include "tempUser.h"

class DataBase
{
public:
	DataBase();
	DataBase(DataBase& other);
	bool isUserExists(string username);
	bool addNewUser(string username, string password, string email);
	bool isUserAndPassMatch(string username, string password);
	vector<string> initQuestions(int questionsNo);
	int insertNewGame();
	bool updateGameStatus(int gameId);
	bool addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime);
	vector<string> getBestScores();
	vector<string> getPersonalStatus(string username);
	~DataBase();
private:
	vector<tempUser> _users;
};

