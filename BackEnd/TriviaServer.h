#pragma once
#include "stadfx.h"
#include "User.h"
#include "Room.h"
#include "DataBase.h"
#include "RecievedMessage.h"
#include "Validator.h"

class TriviaServer
{
private:

	SOCKET _socket;
	map<SOCKET, User*> _connectedUsers;
	DataBase _db;
	map<int, Room*> _roomsList;
	mutex _mtxRecivedMessages;
	queue<RecievedMessage*> _queRcvMessages;
	vector<thread*> _handlers;
	condition_variable _queueEmpty;

	void bindAndListen();
	void accept();
	void clientHandler(SOCKET);
	void safeDeleteUser(RecievedMessage*);
	
	User* handleSignin(RecievedMessage* msg);
	bool handleSignup(RecievedMessage* msg);
	void handleSignout(RecievedMessage* msg);

	void handleLeaveGame(RecievedMessage* msg);
	void handleStartGame(RecievedMessage* msg);
	void handlePlayerAnswer(RecievedMessage* msg);

	bool handleCreateRoom(RecievedMessage*);
	bool handleCloseRoom(RecievedMessage*);
	bool handleJoinRoom(RecievedMessage*);
	bool handleLeaveRoom(RecievedMessage*);
	void handleGetUsersIn(RecievedMessage*);
	void handleGetRooms(RecievedMessage*);

	void handleGetBestScores(RecievedMessage*);
	void handleGetPersonalStatus(RecievedMessage*);

	void handleRecievedMessages();
	void addRecievedMessages(RecievedMessage*);
	RecievedMessage* buildRecieveMessage(SOCKET, int);

	User* getUserByName(string);
	User* getUserBySocket(SOCKET);
	Room* getRoomById(int);
public:
	TriviaServer();
   ~TriviaServer();
   void server();
};

