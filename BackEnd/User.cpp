#include "User.h"
#include "Room.h"

User::User()
{
}

User::~User()
{
}

User::User(string username, SOCKET sock)
{
	this->_username = username;
	this->_sock = sock;
}

void User::send(string message)
{
	try 
	{
		Helper::sendData(this->_sock, message);
	}
	catch (exception& e)
	{
		cout << message << endl;
		cout << e.what() << endl;
	}

}

string User::getUsername()
{
	return this->_username;
}

SOCKET User::getSocket()
{
	return this->_sock;
}

Room* User::getRoom()
{
	return this->_currRoom;
}

Game* User::getGame()
{
	return this->_currGame;
}

void User::setGame(Game* gm)
{
	this->_currRoom = nullptr;
	this->_currGame = gm;
}

void User::clearRoom()
{
	this->_currRoom = nullptr;
}

bool User::createRoom(int roomId, string roomName, int maxUsers, int questionsNo, int questionTime)
{
	if (this->_currRoom != nullptr)
	{
		Helper::sendData(this->_sock, std::to_string(CREATE_ROOM_RESPONSE) + "1");
		return false;
	}

	this->_currRoom = new Room(roomId, this, this->_username, maxUsers, questionsNo, questionTime);
	Helper::sendData(this->_sock, std::to_string(CREATE_ROOM_RESPONSE) + "0");

	return true;
}

bool User::joinRoom(Room* room)
{
	if (this->_currRoom != nullptr)
	{
		return false;
	}

	bool success = room->joinRoom(this);

	if (success)
	{
		return true;
	}

	return false;
}

void User::leaveRoom()
{
	if (this->_currRoom != nullptr)
	{
		this->_currRoom->leaveRoom(this);
		this->_currRoom = nullptr;
	}
}

int User::closeRoom()
{
	if (this->_currRoom == nullptr)
	{
		return -1;
	}

	int roomId = this->_currRoom->closeRoom(this);

	return roomId;
}

bool User::leaveGame()
{
	if (this->_currGame != nullptr)
	{
		//this->_currGame->leaveGame(this);
		this->_currGame = nullptr;
		return true;
	}

	return false;
}

bool User::operator==(User& other)
{
	if (this->_username == other.getUsername())
	{
		return true;
	}

	return false;
}