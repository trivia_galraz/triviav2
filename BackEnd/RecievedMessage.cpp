#include "RecievedMessage.h"



RecievedMessage::RecievedMessage(SOCKET socket, int messageCode)
{
	this->_socket = socket;
	this->_messageCode = messageCode;
}

RecievedMessage::RecievedMessage(SOCKET socket, int messageCode, vector<string> values)
{
	this->_socket = socket;
	this->_messageCode = messageCode;
	this->_values = values;
}


RecievedMessage::~RecievedMessage()
{
	if (this->_user != nullptr)
	{
		this->_user->~User();
	}
	this->_values.clear();
}

SOCKET RecievedMessage::getSock()
{
	return this->_socket;
}

User * RecievedMessage::getUser()
{
	return this->_user;
}

void RecievedMessage::setUser(User* user)
{
	this->_user = user;
}

int RecievedMessage::getMessageCode()
{
	return this->_messageCode;
}

vector<string>& RecievedMessage::getValues()
{
	return this->_values;
}