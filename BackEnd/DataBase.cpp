#include "DataBase.h"

DataBase::DataBase()
{
}

DataBase::DataBase(DataBase& other)
{
	this->_users = other._users;
}

DataBase::~DataBase()
{
	this->_users.clear();
}

bool DataBase::isUserExists(string username)
{
	for (int i = 0; i < this->_users.size(); i++)
	{
		if (this->_users[i]._name == username)
		{
			return true;
		}
	}
	return false;
}

bool DataBase::addNewUser(string username, string password, string email)
{
	if (!this->isUserExists(username))
	{
		this->_users.push_back(tempUser(username, password, email));
		return true;
	}

	return false;
}

bool DataBase::isUserAndPassMatch(string username, string password)
{
	for (auto it = this->_users.begin(); it != this->_users.end(); it++)
	{
		if ((*it)._name == username)
		{
			if ((*it)._password == password)
			{
				return true;
			}
		}
	}

	return false;
}

vector<string> DataBase::initQuestions(int questionsNo) //need to complete!
{
	vector<string> i;
	return i;
}

int DataBase::insertNewGame() //need to complete!
{
	return 0;
}

bool DataBase::updateGameStatus(int gameId) //need to complete!
{
	return false;
}

//need to complete!
bool DataBase::addAnswerToPlayer(int gameId, string username, int questionId, string answer, bool isCorrect, int answerTime)
{
	return true;
}

vector<string> DataBase::getBestScores() //need to complete
{
	vector<string> str;
	return str;
}

vector<string> DataBase::getPersonalStatus(string username) //need to complete
{
	vector<string> str;
	return str;
}