﻿namespace Frontend
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Username = new System.Windows.Forms.Label();
            this.Password = new System.Windows.Forms.Label();
            this.UsernameText = new System.Windows.Forms.TextBox();
            this.PasswordText = new System.Windows.Forms.TextBox();
            this.vldUsername = new System.Windows.Forms.Label();
            this.vldPassword = new System.Windows.Forms.Label();
            this.SignIn = new System.Windows.Forms.Button();
            this.SignUp = new System.Windows.Forms.Button();
            this.JoinRoom = new System.Windows.Forms.Button();
            this.CreateRoom = new System.Windows.Forms.Button();
            this.MyStatus = new System.Windows.Forms.Button();
            this.Quit = new System.Windows.Forms.Button();
            this.BestScores = new System.Windows.Forms.Button();
            this.CurrUsernameLbl = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Frontend.Properties.Resources.kahootLogo;
            this.pictureBox1.Location = new System.Drawing.Point(179, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(334, 132);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Username
            // 
            this.Username.AutoSize = true;
            this.Username.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Username.Location = new System.Drawing.Point(134, 167);
            this.Username.Name = "Username";
            this.Username.Size = new System.Drawing.Size(97, 23);
            this.Username.TabIndex = 1;
            this.Username.Text = "User name";
            // 
            // Password
            // 
            this.Password.AutoSize = true;
            this.Password.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Password.Location = new System.Drawing.Point(134, 198);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(88, 23);
            this.Password.TabIndex = 2;
            this.Password.Text = "Password";
            // 
            // UsernameText
            // 
            this.UsernameText.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsernameText.Location = new System.Drawing.Point(237, 167);
            this.UsernameText.Name = "UsernameText";
            this.UsernameText.Size = new System.Drawing.Size(276, 31);
            this.UsernameText.TabIndex = 3;
            this.UsernameText.Text = "user name goes here...";
            // 
            // PasswordText
            // 
            this.PasswordText.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PasswordText.Location = new System.Drawing.Point(237, 198);
            this.PasswordText.Name = "PasswordText";
            this.PasswordText.PasswordChar = '*';
            this.PasswordText.Size = new System.Drawing.Size(276, 31);
            this.PasswordText.TabIndex = 4;
            // 
            // vldUsername
            // 
            this.vldUsername.AutoSize = true;
            this.vldUsername.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vldUsername.ForeColor = System.Drawing.Color.Red;
            this.vldUsername.Location = new System.Drawing.Point(520, 173);
            this.vldUsername.Name = "vldUsername";
            this.vldUsername.Size = new System.Drawing.Size(0, 23);
            this.vldUsername.TabIndex = 5;
            // 
            // vldPassword
            // 
            this.vldPassword.AutoSize = true;
            this.vldPassword.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.vldPassword.ForeColor = System.Drawing.Color.Red;
            this.vldPassword.Location = new System.Drawing.Point(520, 205);
            this.vldPassword.Name = "vldPassword";
            this.vldPassword.Size = new System.Drawing.Size(0, 23);
            this.vldPassword.TabIndex = 6;
            // 
            // SignIn
            // 
            this.SignIn.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SignIn.Location = new System.Drawing.Point(152, 239);
            this.SignIn.Name = "SignIn";
            this.SignIn.Size = new System.Drawing.Size(361, 51);
            this.SignIn.TabIndex = 7;
            this.SignIn.Text = "Sign In";
            this.SignIn.UseVisualStyleBackColor = true;
            this.SignIn.Click += new System.EventHandler(this.SignIn_Click);
            // 
            // SignUp
            // 
            this.SignUp.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SignUp.Location = new System.Drawing.Point(152, 296);
            this.SignUp.Name = "SignUp";
            this.SignUp.Size = new System.Drawing.Size(361, 51);
            this.SignUp.TabIndex = 8;
            this.SignUp.Text = "Sign Up";
            this.SignUp.UseVisualStyleBackColor = true;
            this.SignUp.Click += new System.EventHandler(this.SignUp_Click);
            // 
            // JoinRoom
            // 
            this.JoinRoom.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.JoinRoom.Location = new System.Drawing.Point(152, 353);
            this.JoinRoom.Name = "JoinRoom";
            this.JoinRoom.Size = new System.Drawing.Size(361, 51);
            this.JoinRoom.TabIndex = 9;
            this.JoinRoom.Text = "Join Room";
            this.JoinRoom.UseVisualStyleBackColor = true;
            this.JoinRoom.Click += new System.EventHandler(this.JoinRoom_Click);
            // 
            // CreateRoom
            // 
            this.CreateRoom.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateRoom.Location = new System.Drawing.Point(152, 410);
            this.CreateRoom.Name = "CreateRoom";
            this.CreateRoom.Size = new System.Drawing.Size(361, 51);
            this.CreateRoom.TabIndex = 10;
            this.CreateRoom.Text = "Create Room";
            this.CreateRoom.UseVisualStyleBackColor = true;
            // 
            // MyStatus
            // 
            this.MyStatus.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MyStatus.Location = new System.Drawing.Point(152, 467);
            this.MyStatus.Name = "MyStatus";
            this.MyStatus.Size = new System.Drawing.Size(361, 51);
            this.MyStatus.TabIndex = 11;
            this.MyStatus.Text = "My Status";
            this.MyStatus.UseVisualStyleBackColor = true;
            // 
            // Quit
            // 
            this.Quit.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Quit.Location = new System.Drawing.Point(270, 581);
            this.Quit.Name = "Quit";
            this.Quit.Size = new System.Drawing.Size(114, 39);
            this.Quit.TabIndex = 12;
            this.Quit.Text = "Quit";
            this.Quit.UseVisualStyleBackColor = true;
            this.Quit.Click += new System.EventHandler(this.Quit_Click);
            // 
            // BestScores
            // 
            this.BestScores.Font = new System.Drawing.Font("Constantia", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BestScores.Location = new System.Drawing.Point(152, 524);
            this.BestScores.Name = "BestScores";
            this.BestScores.Size = new System.Drawing.Size(361, 51);
            this.BestScores.TabIndex = 13;
            this.BestScores.Text = "Best Scores";
            this.BestScores.UseVisualStyleBackColor = true;
            // 
            // CurrUsernameLbl
            // 
            this.CurrUsernameLbl.AutoSize = true;
            this.CurrUsernameLbl.Font = new System.Drawing.Font("Constantia", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CurrUsernameLbl.Location = new System.Drawing.Point(13, 12);
            this.CurrUsernameLbl.Name = "CurrUsernameLbl";
            this.CurrUsernameLbl.Size = new System.Drawing.Size(0, 18);
            this.CurrUsernameLbl.TabIndex = 14;
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(723, 643);
            this.Controls.Add(this.CurrUsernameLbl);
            this.Controls.Add(this.BestScores);
            this.Controls.Add(this.Quit);
            this.Controls.Add(this.MyStatus);
            this.Controls.Add(this.CreateRoom);
            this.Controls.Add(this.JoinRoom);
            this.Controls.Add(this.SignUp);
            this.Controls.Add(this.SignIn);
            this.Controls.Add(this.vldPassword);
            this.Controls.Add(this.vldUsername);
            this.Controls.Add(this.PasswordText);
            this.Controls.Add(this.UsernameText);
            this.Controls.Add(this.Password);
            this.Controls.Add(this.Username);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Menu";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label Username;
        private System.Windows.Forms.Label Password;
        private System.Windows.Forms.TextBox UsernameText;
        private System.Windows.Forms.TextBox PasswordText;
        private System.Windows.Forms.Label vldUsername;
        private System.Windows.Forms.Label vldPassword;
        private System.Windows.Forms.Button SignIn;
        private System.Windows.Forms.Button SignUp;
        private System.Windows.Forms.Button JoinRoom;
        private System.Windows.Forms.Button CreateRoom;
        private System.Windows.Forms.Button MyStatus;
        private System.Windows.Forms.Button Quit;
        private System.Windows.Forms.Button BestScores;
        private System.Windows.Forms.Label CurrUsernameLbl;
    }
}

