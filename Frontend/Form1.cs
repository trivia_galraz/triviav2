﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Frontend
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
            this.JoinRoom.Enabled = false;
            this.CreateRoom.Enabled = false;
            this.MyStatus.Enabled = false;
            this.BestScores.Enabled = false;
            //Thread manage = new Thread(Manage);
            //manage.Start();
            //TcpClient client = new TcpClient();
            //IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            //client.Connect(serverEndPoint);
            //NetworkStream clientStream = client.GetStream();
            //byte[] buffer = new byte[4];
            //buffer = new ASCIIEncoding().GetBytes("20003gal041234");
            //clientStream.Write(buffer, 0, buffer.Length);
            //clientStream.Flush();
        }


        private void Quit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void JoinRoom_Click(object sender, EventArgs e)
        {

        }

        private void SignIn_Click(object sender, EventArgs e)
        {
            
        }

        private void SignUp_Click(object sender, EventArgs e)
        {
            this.Hide();
            SignUpForm wnd = new SignUpForm();
            wnd.ShowDialog();
        }

        //private void Manage()
        //{
        //    TcpClient client = new TcpClient();
        //    IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);

        //    try
        //    {
        //        client.Connect(serverEndPoint);

        //        NetworkStream clientStream = client.GetStream();
        //        byte[] bufferIn;
        //        int bytesRead = 0;
        //        string input;

        //        byte[] buffer = new ASCIIEncoding().GetBytes("20004razN081234RazN");
        //        clientStream.Write(buffer, 0, 4);
        //        clientStream.Flush();

        //        do
        //        {
        //            bufferIn = new byte[4096];
        //            bytesRead = clientStream.Read(bufferIn, 0, 4096);
        //            input = new ASCIIEncoding().GetString(bufferIn);

        //            switch (input[0])
        //            {
        //                case '0':
        //                    break;

        //                case '1':
        //                    break;

        //                case '2':
        //                    break;

        //                default:
        //                    break;
        //            }

        //        } while (input[0] != 2);
        //    }
        //    catch (Exception e)
        //    {
        //        MessageBox.Show(e.Data.ToString());
        //        this.Close();
        //    }
        //}
    }
}
